import React from 'react';
import {
    Dimensions,
    ImageBackground,
    StatusBar,
    ScrollView,
    Button,
    StyleSheet,
    TouchableWithoutFeedback
} from 'react-native';
import {Text, View, Body, Card, CardItem, Icon, Left, Right} from 'native-base';
import RequestHandler from './services/RequestHandler';
import PulseLoader from './loading/PulseLoader'

const {width, height} = Dimensions.get('screen');

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {isLoading: true, covidData: {}, isErrorInReq: false};
    }

    componentDidMount() {
        this.sendReq();
    }

    sendReq() {
        RequestHandler.getCovid()
            .then(data => {
                if (data) {
                    this.setState({
                        covidData: data,
                        isLoading: false
                    });
                } else {
                    this.setState({
                        isErrorInReq: true
                    });
                }
            });
    }

    btnAction = () => {
        this.setState({
            isLoading: true
        });
        this.sendReq();
    };


    renderInputs = (obj) => {
        return (
            <View key={obj.id.toString()}>
                <TouchableWithoutFeedback>
                    <Card style={{flex: 1, opacity: .6}}>
                        <CardItem>
                            <Icon iconFamily="font-awesome" style={{color: '#000000'}}
                                  name="ios-medkit"/>
                            <Body style={{
                                flexDirection: 'column',
                            }}>
                                <Text h7 style={{
                                    color: 'green',
                                    fontWeight: 'bold',
                                    paddingBottom: 5
                                }}>{obj.hospital.name} ({obj.hospital.name_si})</Text>
                                <Text>All Locals: {obj.cumulative_local}</Text>
                                <Text>All Foreigners: {obj.cumulative_foreign} </Text>
                                <Text>Treatment Local: {obj.treatment_local}</Text>
                                <Text>Treatment Foreigners: {obj.treatment_foreign}</Text>
                                <Text>All Patients: {obj.cumulative_total} </Text>
                                <Text>All Treatments: {obj.treatment_total}</Text>
                                <Text style={{paddingTop: 5}}>Last Update: {obj.created_at}</Text>
                            </Body>
                        </CardItem>
                    </Card>
                </TouchableWithoutFeedback>
            </View>
        );
    };

    render() {

        if (this.state.isLoading) {
            return (
                <PulseLoader/>
            )
        }

        if (this.state.isErrorInReq) {
            return (
                <Text>Error in request</Text>
            )
        }

        return (
            <View style={{paddingTop: StatusBar.currentHeight}}>
                <ImageBackground source={require('./assets/splash.jpg')}
                                 style={styles.backgroundImage}/>
                <ScrollView
                    showsVerticalScrollIndicator={false}>

                    <View key={'all'}>
                        <Card style={{opacity: .6}}>
                            <CardItem>
                                <Body style={{
                                    flexDirection: 'column',
                                }}>
                                    <Text h5 style={{color: 'red', fontWeight: 'bold', paddingBottom: 5}}>SUMMARY</Text>
                                    <Text style={styles.txtPadding}>* Locals New
                                        Cases: {this.state.covidData.data.local_new_cases}</Text>
                                    <Text style={styles.txtPadding}>* Total
                                        Locals: {this.state.covidData.data.local_total_cases}</Text>
                                    <Text style={styles.txtPadding}>* Local Individuals In
                                        Hospitals: {this.state.covidData.data.local_total_number_of_individuals_in_hospitals} </Text>
                                    <Text style={styles.txtPadding}>* Locals
                                        Recovered: {this.state.covidData.data.local_recovered}</Text>
                                    <Text style={styles.txtPadding}>* Local
                                        Deaths: {this.state.covidData.data.local_deaths}</Text>
                                    <Text style={styles.txtPadding}>* Local New
                                        Deaths: {this.state.covidData.data.local_new_deaths}</Text>
                                    <Text style={styles.txtPadding}>* Treatment
                                        Local: {this.state.covidData.data.local_deaths}</Text>
                                    <Text style={styles.txtPadding}>* Global New
                                        Cases: {this.state.covidData.data.global_new_cases}</Text>
                                    <Text style={styles.txtPadding}>* Global
                                        Cases: {this.state.covidData.data.global_total_cases}</Text>
                                    <Text style={styles.txtPadding}>* Global
                                        Deaths: {this.state.covidData.data.global_deaths} </Text>
                                    <Text style={styles.txtPadding}>* Global New
                                        Deaths: {this.state.covidData.data.global_new_deaths}</Text>
                                    <Text style={styles.txtPadding}>* Global
                                        Recovered: {this.state.covidData.data.global_recovered}</Text>
                                </Body>
                            </CardItem>
                            <CardItem footer>
                                <Left>
                                    <Text>Last Update: {this.state.covidData.data.update_date_time}</Text>
                                </Left>
                                <Right>
                                    <Button onPress={this.btnAction} title={'Refresh'}><Text>Refresh</Text></Button>
                                </Right>
                            </CardItem>
                        </Card>
                    </View>

                    {this.state.covidData.data.hospital_data.map((obj, index) => (
                        this.renderInputs(obj)
                    ))}

                    <Text>Developed by Hasaka</Text>
                </ScrollView>

            </View>
        );
    }

}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        height: height,
        width: width,
        position: 'absolute',
        bottom: 0,
        opacity: 0.8
    },
    txtPadding: {
        paddingLeft: 5
    }

});