let checkStatus = (response) => {
    if (response.status >= 200 && response.status < 300) {
        return response
    } else {
        let error = new Error(response.statusText);
        error.response = response;
        throw error
    }
};

class RequestGenerator {
    async getRequest(url) {
        let data = await fetch(url, {
        })
            .then(checkStatus)
            .then((response) => {
                return response.json();
            })
            .catch(error => {
                console.error('Error in request');
            });

        return data;
    }
}


export default new RequestGenerator()