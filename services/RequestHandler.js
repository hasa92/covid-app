import RequestGenerator from './RequestGenerator'
import AppConstants from "../utils/AppConstants";

class RequestHandler {
    getCovid() {
        var url = `${AppConstants.HostName}/api/get-current-statistical`;
        console.log(url);
        return RequestGenerator.getRequest(url);
    }
}

export default new RequestHandler()